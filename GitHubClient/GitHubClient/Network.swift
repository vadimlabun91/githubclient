//
//  Network.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import Foundation
import UIKit

class Network {
    let token: String = "cc6c3a88a990089d50eee6e93af47d72cfa490a0"
    
    enum API {
        case users(since: Int?)
        case user(name: String)
        
        var path: String {
            switch self {
            case .users(let since):
                if let offset = since {
                    return "https://api.github.com/users?since=\(offset)"
                } else {
                    return "https://api.github.com/users"
                }
            case .user(let name):
                return "https://api.github.com/users/\(name)"
            }
        }
    }
    
    func get<T: Decodable>(type: T.Type, from api: API, completionHandler: @escaping (T)->()) {
        guard let url = URL(string: api.path) else { return}
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.addValue("token \(token)", forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request) { (json, response, error) in
            guard let json = json else { return }
            let objects = try! JSONDecoder().decode(T.self, from: json)
            DispatchQueue.main.async {
                completionHandler(objects)
            }
        }
        task.resume()
    }
    
    func getImage(url: String, complition: @escaping (UIImage)->()) {
        
        guard let url = URL(string: url) else { return}
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (image, response, error) in
            guard let image = image else { return }
            DispatchQueue.main.async {
                let image = UIImage(data: image)
                complition(image!)
            }
        }
        task.resume()
    }
    
}
