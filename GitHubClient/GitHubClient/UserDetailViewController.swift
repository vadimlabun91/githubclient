//
//  UserDetailViewController.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    var user: User?
    let network = Network()
    
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userEmail: UILabel!
    @IBOutlet var userOrganization: UILabel!
    @IBOutlet var dataStarred: UILabel!
    @IBOutlet var following: UILabel!
    @IBOutlet var followers: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let user = user else { return }
        network.getImage(url: user.avatar_url) { (image) in
            self.userAvatar.image = image
        }
        userName.text = "Login: \(user.login)"
        network.get(type: UserDetails.self, from: .user(name: user.login)) { (user) in
            self.followers.text = "Followers: \(user.followers)"
            self.following.text = "Following: \(user.following)"
            self.userOrganization.text = "Organization: \(user.company ?? "n/a")"
            self.userEmail.text = "Email: \(user.email ?? "n/a")"
            self.dataStarred.text = "\(user.created_at ?? "n/a")"
        }
    }


}
