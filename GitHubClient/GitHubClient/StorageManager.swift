//
//  StorageManager.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import Foundation
import RealmSwift

class StorageManager {
    
    static func saveObject(_ users: [User]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(users, update: .modified)
        }
    }
}
