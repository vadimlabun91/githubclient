//
//  UserDetails.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import RealmSwift

class UserDetails: Object, Codable {
    
    @objc dynamic var followers = 0
    @objc dynamic var following = 0
    @objc dynamic var company: String? = ""
    @objc dynamic var email: String? = ""
    @objc dynamic var created_at: String?
}

