//
//  UserTableViewCell.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var idLabel: UILabel!
}
