//
//  User.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import Foundation
import RealmSwift


class User: Object, Codable {
    @objc dynamic var id = 0
    @objc dynamic var login = ""
    @objc dynamic var avatar_url = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
