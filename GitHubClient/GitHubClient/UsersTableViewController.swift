//
//  UsersTableViewController.swift
//  GitHubClient
//
//  Created by Vadim Labun on 1/16/20.
//  Copyright © 2020 Vadim Labun. All rights reserved.
//

import UIKit
import RealmSwift

class UsersTableViewController: UITableViewController {
    
    var usersResults: Results<User>!
    let network = Network()
    var notificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usersResults = try! Realm().objects(User.self).sorted(byKeyPath: "id")
        
        notificationToken = usersResults.observe { realm in
            self.tableView.reloadData()
        }
        
        network.get(type: [User].self, from: .users(since: nil)) { (users) in
            StorageManager.saveObject(users)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersResults.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserTableViewCell
        let user = usersResults[indexPath.row]
        cell.idLabel.text = "\(user.id)"
        cell.nameLabel.text = "\(user.login)"
        network.getImage(url: user.avatar_url) { (image) in
            cell.avatarImageView.image = image
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = usersResults[indexPath.row]
        performSegue(withIdentifier: "userDetail", sender: user)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 == usersResults.count {
            if let id = usersResults.last?.id {
                network.get(type: [User].self, from: .users(since: id)) { (users) in
                    StorageManager.saveObject(users)
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userDetail" {
            let vc = segue.destination as? UserDetailViewController
            vc?.user = sender as? User
        }
    }
    
}
